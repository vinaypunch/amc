$(function(){
	// external js: isotope.pkgd.js

	// init Isotope
	var $grid = $('.all-listings').isotope({
	  itemSelector: '.listing'
	});

	// store filter for each group
	var filters = {};

$('.filters').on( 'change', function( event ) {
  var $select = $( event.target );
  // get group key
  var filterGroup = $select.attr('value-group');

  // set filter for group
  filters[ filterGroup ] = event.target.value;
  // combine filters
  var filterValue2 = concatValues( filters );

	if(filterGroup === 'Category'){
		 
		var splitValue = filterValue2.split(".");
		var filtervalue3 = '';
		var filtermaincat = filters[ filterGroup ] = event.target.value;

		  
		if(jQuery.inArray("Southland", splitValue) !== -1){
			 filters = {};
			 filtervalue3 = filtermaincat+'.Southland';
			 var filterValue = filtermaincat+'.Southland';
			 filters['Category'] = event.target.value;
			 filters['Region'] = '.Southland';
		} else if(jQuery.inArray("QueenstownLakes", splitValue) !== -1){
			filters = {};
			filtervalue3 = filtermaincat+'.QueenstownLakes';
			var filterValue = filtermaincat+'.QueenstownLakes';
			filters['Category'] = event.target.value;
			filters['Region'] = '.QueenstownLakes';
		} else if(jQuery.inArray("CentralOtago", splitValue) !== -1){
			filters = {};
			filtervalue3 = filtermaincat+'.CentralOtago';
			var filterValue = filtermaincat+'.CentralOtago';
			filters['Category'] = event.target.value;
			filters['Region'] = '.CentralOtago';
		} else if(jQuery.inArray("Waitaki", splitValue) !== -1){
			filters = {};
			filtervalue3 = filtermaincat+'.Waitaki';
			var filterValue = filtermaincat+'.Waitaki';
			filters['Category'] = event.target.value;
			filters['Region'] = '.Waitaki';
		} else if(jQuery.inArray("Dunedin", splitValue) !== -1){
			filters = {};
			filtervalue3 = filtermaincat+'.Dunedin';
			var filterValue = filtermaincat+'.Dunedin';
			filters['Category'] = event.target.value;
			filters['Region'] = '.Dunedin';
		} else if(jQuery.inArray("Clutha", splitValue) !== -1){
			filters = {};
			filtervalue3 = filtermaincat+'.Clutha';
			var filterValue = filtermaincat+'.Clutha';
			filters['Category'] = event.target.value;
			filters['Region'] = '.Clutha';
		 } 
		else{
			filters = {};
			filters[ filterGroup ] = event.target.value;
			var filterValue = concatValues( filters );	
		}
	} else{
		var filterValue = concatValues( filters );
	}
	  // set filter for Isotope
		
		//console.log(filterValue);
		//console.log(filtervalue3);

		$grid.isotope({ filter: filterValue });
		window.scrollTo(0, 0);
     
});
	
	

// flatten object by concatting values
function concatValues( obj ) {
  var value = '';
  for ( var prop in obj ) {
    value += obj[ prop ];
  }
  return value;
}

});	

