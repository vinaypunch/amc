<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = ['randomNumber', 'CurrentYear', 'ServiceTypePages', 'AdditionalTeamPages'];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
            $detect = new Mobile_Detect;
            if ( $detect->isMobile() ) {
                $this->IsMobile = 1;
            }else{
                $this->IsMobile = 0;
            }

            if ( $detect->isTablet() ) {
                $this->isTablet = 1;
            }else{
                $this->isTablet = 0;
            }
        }

        public function randomNumber()
        {
            $randomNumber = rand();
            return $randomNumber;
        }

        public function CurrentYear()
        {
            $year = date("Y");
            return $year;
        }

        public function StaffPages()
        {
            $StaffPages = StaffPage::get()->filter(['DontShow' => '0'])->sort('DisplayOrder', 'ASC');
            return $StaffPages;
        }

        public function MenuStaffPages()
        {
            $StaffPages = StaffPage::get()->filter(['ShowInMenus' => '1'])->sort('DisplayOrder', 'ASC');
            return $StaffPages;
        }

        public function ServiceTypePages()
        {
            $ServiceTypePages = ServiceTypePage::get();
            return $ServiceTypePages;
        }
    }
}
