<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class Slide extends DataObject {

    private static $table_name = 'Slides';

    private static $has_many = [
        'SliderImages' => Image::class
    ];

    private static $summary_fields = [
        'Heading',
    ];

    private static $has_one = [
        'Page' => SiteTree::class
    ];

	private static $db = [
        'Heading' => 'Varchar',
        'SubHeading' => 'Varchar',
        'SlideLinkOne' => 'Varchar',
        'SlideTextOne' => 'Varchar',
        'SlideLinkTwo' => 'Varchar',
        'SlideTextTwo' => 'Varchar',
        'SortOrder' => 'Int',
    ];

    private static $owns = [
        'SliderImages',
        'Page'
    ];

    public function getCMSFields()
    {
        return FieldList::create(

            UploadField::create('SliderImages', 'Images used in Slider on Home Page. 1280px wide recommended.'),
            TextField::create('Heading', 'Heading'),
            TextField::create('SubHeading', 'SubHeading'),
            TextField::create('SlideLinkOne', 'SlideLinkOne'),
            TextField::create('SlideTextOne', 'SlideTextOne'),
            TextField::create('SlideLinkTwo', 'SlideLinkTwo'),
            TextField::create('SlideTextTwo', 'SlideTextTwo')

        );
    }
}
