<?php


use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\ListboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\View\ArrayData;
use SilverStripe\ORM\ArrayList;


class TeamPage extends Page
{
	private static $table_name = 'TeamPage';
	private static $can_be_root = false;

	private static $db = [
        'Position' => 'Varchar',
        'Description' => 'HTMLText',
        'DoctorsLink' => 'Varchar(255)',
        'Qualifications' => 'Varchar'
	];

    private static $has_one = [
        'StaffImage' => Image::class,
        'CandidStaffImage' => Image::class,
         'SliderImage' => Image::class
  	];

    
     private static $owns = [
		 'StaffImage',
         'SliderImage',
         'CandidStaffImage'
  	];

	public function getCMSFields()
	{
		
			$fields = parent::getCMSFields();
			$fields->addFieldToTab("Root.StaffDetails", new TextField('Position'));
            $fields->addFieldToTab("Root.StaffDetails", new TextField('Qualifications'));
            $fields->addFieldToTab("Root.DoctorsLink", new TextField('DoctorsLink', 'This is the doctors specific link in ConnectMed'));
			$fields->addFieldToTab("Root.StaffDetails", new HTMLEditorField('Description'));
			$fields->addFieldToTab("Root.StaffDetails", new UploadField('StaffImage', 'Images should be exactly 255px wide by 311px high'));
            $fields->addFieldToTab("Root.StaffDetails", new UploadField('CandidStaffImage', 'Images should be exactly 255px wide by 311px high'));
            $fields->addFieldToTab("Root.Main", new UploadField('SliderImage'),'Metadata');

		return $fields;
	}
	
}
