<?php

use SilverStripe\Admin\ModelAdmin;

class EmergencySlideAdmin extends ModelAdmin 
{

    private static $managed_models = [
        'EmergencySlide',
    ];

    private static $url_segment = 'emergency-slide';

    private static $menu_title = 'Emergency Slide Admin';
}