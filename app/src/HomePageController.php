<?php

namespace {

    use SilverStripe\Blog\Model\BlogPost; 
    
    class HomePageController extends PageController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = ['GetEmergencySlide','BlogPosts', 'trimString', 'BackGroundImage', 'BackGroundImageLarge'];

        protected function init()
        {
            parent::init();
        }

        public function BlogPosts()
        {
            $BlogPosts = BlogPost::get()->limit(3);
            return $BlogPosts;
        }

        public function GetEmergencySlide()
        {
            $GetEmergencySlide = EmergencySlide::get()->limit(1);
            return $GetEmergencySlide;
        }

        public function trimString($string, $char)
        {
            $rest = substr($string, 0, $char);
            
            return $rest;
        }

        public function BackGroundImage($relationship, $function , $mobileSize, $tabletSize, $desktopSize)
        {
            $detect = new Mobile_Detect;

            if($detect->isMobile()){

                return $this->$relationship->$function($mobileSize)->URL;

            }elseif($detect->isTablet()){

                return $this->$relationship->$function($tabletSize)->URL;

            }else{

                return $this->$relationship->$function($desktopSize)->URL;
            }
        }

        public function BackGroundImageLarge($relationship, $function , $mobileSize, $tabletSize, $desktopSize)
        {
            $detect = new Mobile_Detect;

            if($detect->isMobile()){

                return $this->$relationship->$function($mobileSize)->URL;

            }elseif($detect->isTablet()){

                return $this->$relationship->$function($tabletSize)->URL;

            }else{

                return $this->$relationship->ScaleWidth($desktopSize)->URL;
            }
        }
        
    }
}
