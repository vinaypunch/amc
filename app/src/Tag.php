<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;

class Tag extends DataObject {

    private static $table_name = 'Tag';

    private static $summary_fields = [
        'PublicName',
        'CodeName'
    ];
    
    private static $has_one = [
        "Page" => SiteTree::class,
    ];

	private static $db = [
        'PublicName' => 'Varchar',
        'CodeName' => 'Varchar'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('PublicName', 'A nice Looking name for the Service - presented at the top of the service tag as a filter'),
            TextField::create('CodeName', 'Name used for code - ie same as public but without spaces')
        );
    }
}
