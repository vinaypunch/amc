<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class ContentBlock extends DataObject {

    private static $table_name = 'ContentBlock';
    private static $default_sort = 'SortOrder';
    private static $has_one = [
        "Page" => SiteTree::class,
        "Image" => Image::class
    ];
	
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'SortOrder' => 'Int'
    ];

    private static $owns = [
        'Page',
        'Image'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Title', 'Title to be shown on page'),
            HTMLEditorField::create('Content', 'Description of Content'),
            UploadField::create('Image', 'Left aligned image if needed (not necessary)')
        );
    }
}
