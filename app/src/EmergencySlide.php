<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class EmergencySlide extends DataObject {

    private static $table_name = 'EmergencySlide';

	private static $db = [
        'Heading' => 'Varchar',
        'SubHeading' => 'Varchar',
        'SlideLinkOne' => 'Varchar',
        'SlideTextOne' => 'Varchar',
        'SlideLinkTwo' => 'Varchar',
        'SlideTextTwo' => 'Varchar'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create('Heading', 'Heading'),
            TextField::create('SubHeading', 'SubHeading'),
            TextField::create('SlideLinkOne', 'SlideLinkOne'),
            TextField::create('SlideTextOne', 'SlideTextOne'),
            TextField::create('SlideLinkTwo', 'SlideLinkTwo'),
            TextField::create('SlideTextTwo', 'SlideTextTwo')

        );
    }
}
