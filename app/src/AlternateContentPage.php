<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;
    use SilverStripe\Forms\CheckboxField;

    class AlternateContentPage extends ContentPage
    {
        private static $db = [
            'ShowContentInstead' => 'Boolean'
        ];

        private static $has_one = [];

        private static $has_many = [
        	'ContentBlocks' => ContentBlock::class
        ];

        private static $owns = [
            'ContentBlocks'
        ];

        public function getCMSFields()
		{
			$fields = parent::getCMSFields();
			$fields->addFieldToTab("Root.Main", new CheckboxField('ShowContentInstead', 'If you select this button the Main Content field will be shown instead of the Content Blocks on this particular page.'));
            $conf = GridFieldConfig_RecordEditor::create(10);
            $conf->addComponent(new GridFieldSortableRows('SortOrder'));
            $fields->addFieldToTab('Root.ContentBlocks', new GridField('ContentBlock', 'ContentBlocks', $this->ContentBlocks(), $conf));
			return $fields;
		}
        
    }
}
