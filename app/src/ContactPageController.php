<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\RequiredFields;
    use SilverStripe\Control\Email\Email;
    use Silverstripe\SiteConfig\SiteConfig;

    class ContactPageController extends PageController
    {
        
        private static $allowed_actions = [''];

     
      /*  public function ContactForm() {

            $fields = new FieldList( 
                TextField::create('FullName')->addExtraClass('form-control')->setAttribute('placeholder','Full Name *'),
                EmailField::create('Email')->addExtraClass('form-control')->setAttribute('placeholder','Email *'),
                TextField::create('Phone')->addExtraClass('form-control')->setAttribute('placeholder','Phone *'), 
                TextareaField::create('Message')->addExtraClass('form-control')->setAttribute('placeholder','Your Message for us *')
            ); 

            $actions = new FieldList( 
                FormAction::create('submit', 'Contact Us')->addExtraClass('btn btn-secondary orange form-control')
            );

            return Form::create($this, 'ContactForm', $fields, $actions)->setTemplate('ContactFormTemplate'); 

        }

        public function submit($data, $form) {
            $email = Email::create('noreply@aspiringmedical.co.nz', 'gena@lawfordandco.com', 'My subject');
		$email->html('<p>My HTML email content</p>');
		$email->text('My plain text email content');
		$email->send();

            $form->sessionMessage('Thank you for your enquiry. We will be in touch as soon as possible.', 'success');

            return $this->redirectBack();
        }*/
    }
}
