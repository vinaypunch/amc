<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use SilverStripe\Forms\ListboxField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\DropdownField;

    class ServiceTypePage extends Page
    {
        private static $db = [
            'ContactMethods' => "Enum('Phone,Email,ConnectMed,ImmigrationMedical,TravelMedical, AviationMedical, LifestyleMedicine,WalkIn, RepeatPrescriptions, None')",
            'WalkInScript' => 'HTMLText'
        ];

        private static $has_one = [
        	'ServiceImage' => Image::class,
            'ServiceImageAlternate' => Image::class
        ];

        private static $has_many = [
            'Tags' => Tag::class
        ];

        private static $many_many = [
            'Doctors' => StaffPage::class
        ];

        private static $owns = [
            'Tags',
            'ServiceImage',
            'ServiceImageAlternate',
            'Doctors'
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
            $source = Tag::get()->map('ID', 'CodeName');
            $fields->addFieldToTab("Root.ServiceDetails", new ListboxField( $name = "Tags", $title = "Add tags for this Service", $source, $value = 1 ));

            $DoctorsSource = StaffPage::get()->map('ID', 'MenuTitle');
            
            if( $this->ContactMethods == 'WalkIn' ){

                $fields->addFieldToTab("Root.ContactMethods", new HTMLEditorField('WalkInScript', 'Script shown when Contact Method shown is WalkIn'));
            }
            
            $fields->addFieldToTab('Root.DoctorDetails', ListboxField::create(
                'Doctors',
                'Select Doctors(s)',
                $DoctorsSource
            ));

            $fields->addFieldToTab('Root.ContactMethods', new DropdownField(
              'ContactMethods',
              'ContactMethods to be shown around the site for this particular service',
              singleton('ServiceTypePage')->dbObject('ContactMethods')->enumValues()
            ));

			return $fields;
		}
    }
}
