<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\TextField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\DropdownField;

    class StaffPage extends Page
    {
        private static $db = [
        	'Position' => 'Varchar',
        	'Description' => 'HTMLText',
            'DoctorsLink' => 'Varchar(255)',
            'Qualifications' => 'Varchar',
            'DisplayOrder'  => "Enum('1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','1')"
        ];

        private static $has_one = [
        	'StaffImage' => Image::class,
            'CandidStaffImage' => Image::class,
            'SliderImage' => Image::class
        ];

        private static $owns = [
            'StaffImage',
            'SliderImage',
            'CandidStaffImage'
        ];

        private static $belongs_many_many = [
            'Doctors' => StaffPage::class
        ];

        public function getCMSFields()
		{
			$fields = parent::getCMSFields();
			$fields->addFieldToTab("Root.StaffDetails", new TextField('Position'));
            $fields->addFieldToTab("Root.StaffDetails", new TextField('Qualifications'));
            $fields->addFieldToTab("Root.DoctorsLink", new TextField('DoctorsLink', 'This is the doctors specific link in ConnectMed'));
			$fields->addFieldToTab("Root.StaffDetails", new HTMLEditorField('Description'));
			$fields->addFieldToTab("Root.StaffDetails", new UploadField('StaffImage', 'Images should be exactly 255px wide by 311px high'));
            $fields->addFieldToTab("Root.StaffDetails", new UploadField('CandidStaffImage', 'Images should be exactly 255px wide by 311px high'));
            $fields->addFieldToTab("Root.Main", new UploadField('SliderImage'));

            $fields->addFieldToTab('Root.StaffDetails', new DropdownField(
              'DisplayOrder',
              'Display Order for this page in menus, team pages etc',
              singleton('StaffPage')->dbObject('DisplayOrder')->enumValues()
            ));

			return $fields;
		}
    }
}