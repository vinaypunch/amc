<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

class CustomSiteConfig extends DataExtension 
{
    
    private static $db = [
        'Phone' => 'Text',
        'Fax' => 'Text',
        'Email' => 'Text',
        'AddressOne' => 'Text',
        'AddressTwo' => 'Text',
        'RegularHours' => 'HTMLText',
        'AfterHours' => 'HTMLText',
        'PhoneDetails' => 'HTMLText',
		'AlertBannerText' => 'Varchar(70)',
		'AlertBannerLink' => 'Text',
		'AlertBannerActive' => 'Int'
    ];

    private static $has_one = [
        'Logo' => Image::class,
        'ScrollLogo' => Image::class
    ];

    private static $owns = [
        'Logo',
        'ScrollLogo'
    ];

    public function updateCMSFields(FieldList $fields) 
    {
        $fields->addFieldToTab("Root.Logo", new UploadField("Logo", "Logo displayed when you have a transparent or non-white background"));
        $fields->addFieldToTab("Root.Logo", new UploadField("ScrollLogo", "Logo displayed when you have a white background"));
        $fields->addFieldToTab("Root.Details", new TextField("Email", "Email"));
        $fields->addFieldToTab("Root.Details", new TextField("AddressOne", "AddressOne"));
        $fields->addFieldToTab("Root.Details", new TextField("AddressTwo", "AddressTwo"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("RegularHours", "RegularHours"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("AfterHours", "AfterHours"));
        $fields->addFieldToTab("Root.Details", new HTMLEditorField("PhoneDetails", "PhoneDetails"));
		$fields->addFieldToTab("Root.AlertBanner", $bannertext = new TextField("AlertBannerText", "Alert Banner Text"));
		$fields->addFieldToTab("Root.AlertBanner", new TextField("AlertBannerLink", "Alert Banner Link (e.g. www.aspiringmedical.co.nz)"));
		$fields->addFieldToTab("Root.AlertBanner", new CheckboxField("AlertBannerActive","Tick box to make alert banner active"));
		
		$bannertext->setMaxLength(70);
		$bannertext->setDescription('70 character limits');

		
    }
}