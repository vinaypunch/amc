<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    class Page extends SiteTree
    {
        private static $db = [
        	'SubHeading' => 'Varchar',
            'DontShow' => 'Boolean',
            "isMobile" => 'Boolean',
            "isTablet" => 'Boolean'
        ];

        private static $has_one = [
            'PageTitleBck' => Image::class
        ];

        private static $owns = [
            'PageTitleBck'
        ];

        public function getCMSFields()
		{
			
			$fields = parent::getCMSFields();
			
			$fields->addFieldToTab("Root.Main", new TextField('SubHeading'));
            $fields->addFieldToTab("Root.Main", new CheckboxField('DontShow', 'If selected this wont show in holder pages'));
            $fields->addFieldToTab("Root.PageTitleBckImage", new UploadField("PageTitleBck", "Image shown behind page title on some but not all pages. Should be 1800px wide."));

			return $fields;
		}
    }
}
