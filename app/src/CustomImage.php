<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DataExtension;

class CustomImage extends DataExtension 
{
    
    private static $db = [
    ];

    private static $has_one = [
        'HomePage' => SiteTree::class,
        'Slide' => Slide::class
    ];

    private static $owns = [
        'HomePage'
    ];
}