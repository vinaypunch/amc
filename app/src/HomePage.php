<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\TextField;
	use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;

    class HomePage extends Page
    {
        private static $db = [
        	'ContentBlockOne' => 'HTMLText',
        	'ContentBlockTwo' => 'HTMLText',
        	'ContentBlockThreeTitle' => 'Text',
			'ContentBlockThree' => 'HTMLText',
        	'ContentBlockFour' => 'HTMLText',
        	'ContentBlockFive' => 'HTMLText',
        	'ContentBlockSix' => 'HTMLText',
        ];

        private static $has_many = [
            'Slides' => Slide::class
        ];

        private static $has_one = [
            'ContentBlockThreeImage' => Image::class,
            'MetroBanner' => Image::class,
            'ASPBanner' => Image::class,
            'TestimonialImage' => Image::class,
            'HomePageSliderImageMobile' => Image::class
        ];

        private static $owns = [
            'Slides',
            'ContentBlockThreeImage',
            'MetroBanner',
            'ASPBanner',
            'TestimonialImage'
        ];

        public function getCMSFields()
		{
			$fields = parent::getCMSFields();
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockOne'));
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockTwo'));
			$fields->addFieldToTab("Root.ContentBlocks", new TextField('ContentBlockThreeTitle'));
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockThree'));
            $fields->addFieldToTab("Root.Images", new UploadField('ContentBlockThreeImage'));
            $fields->addFieldToTab("Root.Images", new UploadField('MetroBanner'));
            $fields->addFieldToTab("Root.Images", new UploadField('ASPBanner'));
            $fields->addFieldToTab("Root.Images", new UploadField('TestimonialImage'));
            $fields->addFieldToTab("Root.MobileSliderImage", new UploadField('HomePageSliderImageMobile'));
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockFour'));
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockFive'));
			$fields->addFieldToTab("Root.ContentBlocks", new HTMLEditorField('ContentBlockSix'));
            
            // $fields->addFieldToTab('Root.Slides', GridField::create(
            //     'Slides',
            //     'Slides',
            //     $this->Slides(),
            //     GridFieldConfig_RecordEditor::create()
            // ));

            $conf = GridFieldConfig_RecordEditor::create(10);
            $conf->addComponent(new GridFieldSortableRows('SortOrder'));
            $fields->addFieldToTab('Root.Slides', new GridField('Slides', 'Slides', $this->Slides(), $conf));

			return $fields;
		}
    }
}
