<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class UserDefinedFormExtension extends DataExtension 
{

    private static $db = [
        'ExtraContent' => 'HTMLText'
    ];

    public function updateCMSFields(FieldList $fields) 
    {
        $fields->addFieldToTab("Root.Right Column", new HTMLEditorField("ExtraContent", "On UserForm Pages (ie contact or Immigration form pages) if you fill this out it will show on the right hand side of the page."));
    }

}
