<!DOCTYPE html>
<html dir="ltr" lang="en-US">
   <% include Head %>
   <body class="stretched $ClassName">
      <div id="wrapper" class="clearfix">
         <% include HeaderAndNavigation %>
         <% include PageTitle %>
         <section id="content" style="margin-bottom: 0px;">
            <div class="content-wrap">
               <div class="container clearfix">
                  <div class="nobottommargin clearfix">
                     <div id="faqs" class="faqs">
                        <div class="row">
                          
                            <% if $SubHeading %>
                              <div class="col-12">
                                <div id="faqs-list" class="fancy-title title-bottom-border">
                                   <h3>$SubHeading:</h3>
                                </div>
                              </div>
                            <% end_if %>
                          
                          <% if $ShowContentInstead %>
                            
                            <div class="col-12">
                              $Content
                            </div>

                          <% else %>

                            <div class="col-12">
                              <div class="row d-flex justify-content-center">
                                
                                <% loop $ContentBlocks %>
                                
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                      
                                      $Image
                                      <h3 class="my-2" id="faq-{$Pos}">$Title</h3>
                                      <span class="CustomBlue">$Content</span>

                                    </div>
                                    
                                <% end_loop %>

                              </div>
                            </div>
                            
                          <% end_if %>
                          
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </section>
         <% include Footer %>
      </div>
      <% include JS %>
   </body>
</html>