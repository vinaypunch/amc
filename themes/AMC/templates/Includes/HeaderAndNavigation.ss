<header id="header">

    <div id="header-wrap">
	 	<% if $SiteConfig.AlertBannerActive == '1' %><div class="alert-banner"><div class="alert-banner-text"><a href="$SiteConfig.AlertBannerLink">$SiteConfig.AlertBannerText</a></div></div><% end_if %>
        <div class="container clearfix">
            
            <!-- SM & MD devices only -->
            <div class="d-lg-none">
                
                <div id="logo">

                    <a href="$BaseHref" class="scrollLogo standard-logo d-flex justify-content-start" data-light-logo="$SiteConfig.ScrollLogo.URL">
                        <img class="mt-3" src="$SiteConfig.ScrollLogo.URL" alt="Aspiring Medical Centre - Wanaka New Zealand">
                    </a>

                </div> 

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <div class="row justify-content-center mobile-buttons">

                    <div class="d-flex justify-content-center">
                        <a href="tel:034430725" class="tel mr-3 mb-0 text-center bkApp"><b>03 443 0725</b></a>
                    </div>
					<div class="d-flex justify-content-center">
                        <a href="{$BaseHref}/patient-info" class="m-0 mb-0 text-center button button-3d button-small bkApp" target="_blank">Enrol</a>
                    </div>
					
					<!--<div class="break"></div>-->
					<div class="d-flex justify-content-center">
                        <a href="https://portal.managemyhealth.co.nz/m/Appointment/Index/6F002F00730072006B003400410053006C0043006F003D00/lc" class="m-0 mb-0 text-center button button-3d button-small bkApp" target="_blank">Book Appointment</a>
                    </div>
					
					
					<div class="d-flex justify-content-center">
                        <a href="{$BaseHref}/wanaka-medical-services/immigration-medicals/" class="m-0 mb-0 text-center button button-3d button-small bkApp" target="_blank">Immigration</a>
                    </div>

                </div>

                <nav id="primary-menu" class="row">
                    
                    <ul class="pl-5 col-12">
                        <% loop $Menu(1) %>
                                    
                            <li class="sub-menu">
                                
                                <a href="$Link" class="sf-with-ul">
                                    <div>$MenuTitle.XML<% if $Children %><i class="icon-caret-right1"></i><% end_if %></div>
                                </a>

                                <% if $Children %>

                                    <ul class="$ClassName">

                                        <% if $ClassName == MeetOurTeamPage %>

                                            <% loop $Up.MenuStaffPages %>

                                                <li class="sub-menu">
                                                    
                                                    <a href="$Link" class="sf-with-ul">
                                                        <div>$MenuTitle.XML<% if $Children %><i class="icon-caret-right1"></i><% end_if %></div>
                                                    </a>

                                                    <% if $Children %>
                                                        <ul>
                                                            <% loop $Children %>
                                                                
                                                                <li>
                                                                    <a href="$Link">

                                                                        <div>$MenuTitle.XML</div>

                                                                    </a>
                                                                </li>

                                                            <% end_loop %>
                                                        </ul>
                                                    <% end_if %>

                                                </li>

                                            <% end_loop %>

                                            <% loop $Children %>

                                                <% if $ClassName != StaffPage %>
                                                    <li class="sub-menu">
                                                        <a href="$Link" class="sf-with-ul">
                                                            <div>$MenuTitle.XML</div>
                                                        </a>
                                                    </li>
                                                <% end_if %>

                                            <% end_loop %>

                                        <% else %>

                                            <% loop $Children %>

                                                <li class="sub-menu">
                                                    
                                                    <a href="$Link" class="sf-with-ul">
                                                        <div>$MenuTitle.XML<% if $Children %><i class="icon-caret-right1"></i><% end_if %></div>
                                                    </a>

                                                    <% if $Children %>
                                                        <ul>
                                                            <% loop $Children %>
                                                                
                                                                <li>
                                                                    <a href="$Link">

                                                                        <div>$MenuTitle.XML</div>

                                                                    </a>
                                                                </li>

                                                            <% end_loop %>
                                                        </ul>
                                                    <% end_if %>

                                                </li>

                                            <% end_loop %>

                                        <% end_if %>

                                    </ul>

                                <% end_if %>

                            </li>
                            
                        <% end_loop %>
                    </ul>

                    <div class="col-2 d-none d-lg-block BookButtonContainer">
                        
                        <a href="https://www.managemyhealth.co.nz/individuals/">
                            <img src="{$BaseHref}/_resources/themes/AMC/amcimages/button-5.png" />
                        </a>

                    </div>

                </nav>
            </div>
            
            <!-- LG and Up Devices  -->
            <div class="d-none d-lg-block">
                <div class="mt-2 row">
                    
                    <div class="col-3">
                        
                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                        
                        <div id="logo">

                            <a href="$BaseHref" data-light-logo="$SiteConfig.ScrollLogo.ScaleHeight(100).URL">
                                <img src="$SiteConfig.ScrollLogo.ScaleHeight(100).URL" alt="Canvas Logo">
                            </a>

                        </div> 

                    </div>

                    <div class="d-flex justify-content-end col">
                        <nav id="primary-menu" class="row">
                            <div class="row ml-auto">
                                <div class="col-3 mt-2 text-right">
                                    <a href="tel:034430725" class="tel">03 443 0725</a>
                                </div>
								<div class="col-2 pr-0 pl-0 text-right">
									<a href="{$BaseHref}/patient-info" class="ml-auto text-center button button-3d button-small bkApp btn-secondary">Enrol</a>
								</div>
                                <div class="col-4 pr-0 text-right">
                                    <a href="https://portal.managemyhealth.co.nz/m/Appointment/Index/6F002F00730072006B003400410053006C0043006F003D00/lc" class="ml-auto text-center button button-3d button-small bkApp btn-secondary" target="_blank">Book Appointment</a>
                                </div>
								<div class="col-3 pr-0 text-right">
                                    <a href="https://www.aspiringmedical.co.nz/wanaka-medical-services/immigration-medicals/" class="ml-auto text-center button button-3d button-small bkApp btn-secondary">Immigration</a>
                                </div>
                            </div>
                            <ul class="d-flex justify-content-end sf-js-enabled col-12 pr-0" style="touch-action: pan-y;">
                                
                                <% loop $Menu(1) %>
                                    
                                    <li class="sub-menu">
                                        
                                        <a href="$Link" class="sf-with-ul <% if $Last %>last-child<% end_if %>">
                                            <div>$MenuTitle.XML</div>
                                        </a>

                                        <% if $Children %>

                                            <ul class="$ClassName">

                                                <% if $ClassName == MeetOurTeamPage %>

                                                       <% loop $Children %>

                                                        <% if $ClassName != StaffPage %>
                                                            <li class="sub-menu">
                                                                <a href="$Link" class="sf-with-ul">
                                                                    <div>$MenuTitle.XML</div>
                                                                </a>
                                                            </li>
                                                        <% end_if %>

                                                    <% end_loop %>

                                                <% else %>

                                                    <% loop $Children %>

                                                        <li class="sub-menu">
                                                            
                                                            <a href="$Link" class="sf-with-ul">
                                                                <div>$MenuTitle.XML<% if $Children %><i class="icon-caret-right1"></i><% end_if %></div>
                                                            </a>

                                                            <% if $Children %>
                                                                <ul>
                                                                    <% loop $Children %>
                                                                        
                                                                        <li>
                                                                            <a href="$Link">

                                                                                <div>$MenuTitle.XML</div>

                                                                            </a>
                                                                        </li>

                                                                    <% end_loop %>
                                                                </ul>
                                                            <% end_if %>

                                                        </li>

                                                    <% end_loop %>

                                                <% end_if %>

                                            </ul>

                                        <% end_if %>

                                    </li>

                                <% end_loop %>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>