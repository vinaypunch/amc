<!DOCTYPE html>
<html dir="ltr" lang="en-US">
   <% include Head %>
   <body class="stretched $ClassName">
      <div id="wrapper" class="clearfix">
         <% include HeaderAndNavigation %>
         
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix addPadding">
                        <div class="clear"></div>

                        <% if $SubHeading %>
                            
                            <div class="fancy-title title-border">

                              <h3>$Subheading</h3>

                            </div>
                            
                        <% end_if %>

                        <div class="row">
                            
                            <div class="col-sm-12 col-md-3 text-center text-md-left">
                                <img src="$StaffImage.ScaleWidth(411).URL" class="img-fluid">
                                
                                
                                <h5>$Qualifications</h5>
								
								<!--<a href="$DoctorsLink" target="_blank">
                                    <button type="button" class="teal bkApp btn btn-secondary mt-3">Book an Appointment</button>
                                </a>-->
                            </div>
                            
                            <div class="col-sm-8">
                            
                                <div class="team-content">
									<h3>$Title</h3>
                                    $Content
                                </div>
                                
                            </div>

                            <div class="clear"></div>

                        </div>
                    </div>
                </div>
            </section>

         <% include Footer %>
      </div>
      <% include JS %>
   </body>
</html>