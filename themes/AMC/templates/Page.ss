<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <% include Head %>
    <body class="stretched $ClassName">
        <div id="wrapper" class="clearfix">
            <% include HeaderAndNavigation %>
            
            $Layout

            <% include Footer %>
        </div>
        <% include JS %>
    </body>
</html>