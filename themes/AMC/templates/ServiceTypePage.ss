<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <% include Head %>
    <body class="stretched $ClassName">
        <div id="wrapper" class="clearfix">
            <% include HeaderAndNavigation %>
            <% include PageTitle %>
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix">
                        <div class="row">
                            <div class="col-lg-4 nobottommargin">
                                
                                <% include ContactMethods %>

                            </div>
                        
                            <div class="col-lg-8 portfolio-single-content col_last nobottommargin">

                                <div class="nobottommargin">
                                    $Content
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <% include Footer %>
        </div>
        <% include JS %>
    </body>
</html>